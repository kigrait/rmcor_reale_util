package com.rmcor.reale.utilites;

/**
 *
 * @author Shailendra yadav
 */
public class Constants {

    public static final String ON_LINE_PRICE = "onLinePrice";
    public static final String SALES_ORDER = "SO";
    public static final String PURCHASE_ORDER = "PO";
    public static final String SUCCESS = "success";
    public static final String ERROR = "error";
    public static final String PUBLIC_SCHEMA_NAME = "public";
    public static final String MRP_PRICE = "mrp";
    public static final Integer ACTIVE_STATUS = 1;
    public static final Integer INACTIVE_STATUS = 0;
    public static final String USER_ADMIN = "admin";

    //When we add/update category/product is public schema by mule/activemq queue.
    public static final String ADD_NEW_CATEGORY = "addNewCategory";
    public static final String UPDATE_CATEGORY = "updateCategory";
    public static final String ADD_NEW_PRODUCT = "addNewProduct";
    public static final String UPDATE_PRODUCT = "updateProduct";
    public static final char OPERATION_ON_CATEGORY = 'c';
    public static final char OPERATION_ON_PRODUCT = 'p';

    //category/product message
    public static final String CATEGORY_ADDED_IN_STORE = "Category added into store successfully";
    public static final String CATEGORY_UPDATED_IN_STORE = "Category updated into store successfully";
    public static final String PRODUCT_ADDED_IN_STORE = "Product added into store successfully";
    public static final String PRODUCT_UPDATED_IN_STORE = "Product updated into store successfully";

    //end
    public static final String PURCHASE_ORDER_PLACED_SUCCESS = "Purchase order placed to store schema successfully";
    public static final String PURCHASE_ORDER_ITEMS_SAVE_SUCCESS = "Purchase order item save into supplier schema";
    public static final String PURCHASE_ORDER_ADDRESS_SAVE_SUCCESS = "Purchase order address save into supplier schema";
    public static final String PURCHASE_ORDER_API_CREDIALTIAL_SAVE_SUCCESS = "Purchase order api crediantial save into supplier schema";
    public static final String ORDER_STATUS_UPDATE_SUCCESS = "Purchase order status update into store schema";
    public static final String ORDER_STATUS_UPDATE_SUCCESS_SUPPLIER = "Purchase order status update into store schema as status changed into netree local supplier schema  by actual supplier  schema...";

    //Walmart supplier authentication detail 
    public static final String NETREE_WALMART_SCHEMA_NAME = "1081603425319261196"; // "1081603425319261196";
    public static final String WALMART_URL_FOR_GET_PRODUCT_DETAIL = "http://www8.martjack.com/developerapi/Product/Information";
    public static final String WALMART_NETREE_MERCHANT_ID = "2f9b7c67-7c32-4267-9132-abb503dc4c83";
    public static final String WALMART_PUBLIC_KEY = "WMRLECG5";
    public static final String WALMART_SECRET_KEY = "Q4P9NP4SNY4QUW1VYXULULUD";
    public static final Integer WALMART_LOCATION_ID_LUCKNOW = 109;
    public static final Integer WALMART_LOCATION_ID_HYDERABAD = 108;

    //before status
    /*DRAFT(0), SUBMITTED(1),FAILED(20), DELIVERED(2), PARTIALLY_RECEIVED(3), CANCELLED(9), COMPLETED(10), ORDER_PLACED_PENDING(18),
        AUTHORIZE(5), READY_FOR_PACKAGING(6), READY_FOR_PICKUP(7), PICKED_UP(8), SHIPPED(11), UNPROCESSED_REFUND(12),
        SHIPPED_RETURNED(13), SHIPPED_REFUND(14), SHIPPED_RETURNED_AND_REFUND(15), DELIVERED_REFUND(16), DELIVERED_RETURNED(17),
        STOCK_PENDING(4), DELIVERED_RETURNED_AND_REFUND(19); */
    //now still updated on 03/01/2016....updated By Anil Sharma
    /* DRAFT(0), SUBMITTED(1), FAILED(20), DELIVERED(2), PARTIALLY_RECEIVED(3), CANCELLED(9), COMPLETED(10), ORDER_PLACED_PENDING(18),
        APPROVED(5), READY_FOR_PACKAGING(6), READY_FOR_PICKUP(7), PICKED_UP(8), SHIPPED(11), UNPROCESSED_REFUND(12),
        SHIPPED_RETURNED(13), SHIPPED_REFUND(14), SHIPPED_RETURNED_AND_REFUND(15), DELIVERED_REFUND(16), DELIVERED_RETURNED(17),
        INVENTORY_PENDING(4), DELIVERED_RETURNED_AND_REFUND(19), PENDING(21);   */
 /*
        * Result Constant 
     */
    public static final String SUCCESSCODE = "S10001";
    public static final String ERRORCODE = "F90001";
    public static final String ALREADYEXISTCODE = "A20007";
    public static final String INVALIDPARAMETERCODE = "E80002";
    public static final String ERROR_OTP_SENT_WITHOUT_ACKNOLWEDGEMENT = "E80005";
    public static final String SIGNINCODE = "S10002";
    public static final String UNAUTHCODE = "A20002";
    public static final String NODATACODE = "S10003";
    public static final String NOTEXIST = "A20003";
    public static final String ALREADYUSEDCODE = "A20006";
    public static final String ERRORCODE_DIN_INVALID = "F91001";

    public static final String ERROR_PRODUCT_NOT_AVAILABLE = "F30001";
    public static final String ERROR_PRODUCT_INVENTORY_NOT_AVAILABLE = "F30002";
    public static final String ERROR_ORDER_PAYMENT_STATUS_NOT_AVAILABLE = "F40001";

    public static final String ERROR_EMAIL_ALREADY_EXISTS = "F91001";
    public static final String ERROR_MOBILE_ALREADY_EXISTS = "F91002";
    public static final String ERROR_GROUP_DOES_NOT_EXISTS = "F91003";
    public static final String ERROR_USER_DOES_NOT_EXISTS = "F91004";
    public static final String ERROR_BUSINESS_DOES_NOT_EXISTS = "F91005";
    public static final String ERROR_CONTACT_DOES_NOT_EXISTS = "F91006";
    public static final String ERROR_PASSWORD_DOES_NOT_MATCH = "F91007";
    public static final String WARNING_MEMBER_ALREADY_EXIST = "A20009";
    
    public static final String WORKING = "working";
    public static final String RESIGNED = "resigned";
    public static final String NOT = "not";

    public static final String CONTACT = "contact";
    public static final String CUSTOMER = "C";
    public static final String BUSINESS = "B";
    public static final String ACTIVE = "active";

    /*
         * status for connection
     */
    public static final String CONNECTED_STATUS = "0";
    public static final String INVITE_STATUS = "1";
    public static final String CONNECT_STATUS = "2";
    public static final String INVITED_STATUS = "3";
    public static final String CONNECTSENT_STATUS = "4";

    public static final String BRAND_OWNER_HO = "10";
    public static final String BRAND_OWNER_BRANCH = "11";
    public static final String BRAND_PARTNER_SUBTYPE = "12";
    public static final String MARKET_PLACE_PRE_INVOICE = "I";
    public static final String MARKET_PLACE_POST_INVOICE = "P";
    public static final String MARKET_INVOICE_SEQ = "market_invoice_seq";

    
    public static final String Status_Yes = "Yes";
    public static final String Status_No = "No";
    public static final String Foop = "Foop";
    public static final String Error_Otp = "sms and email status is require";
    public static final String Error_One_Time_Check = "Please enter valid OTP";
    public static final String Details_Status_Value = "Details is not present";

    public static final String Otp_Error_Expire = "OTP is expire";
    public static final String Otp_Attempt = "You can complete all attempt";
    public static final Integer THIRTY_MINUTES = 30 * 60 * 1000;
    public static final Integer Attempt_Resend_Otp = 3;
    public static final String Resend_Status = "fail";
    public static final String Details_Status = "D80001";
    public static final String Registration_Error = "Error in registration of user";
    public static final String Mobile_Number_Exist = "Mobile number already register in system";
    public static final String Already_Exist_Code = "409";
    
    
    public enum INPUT_TYPE {
        LIST(0, "List"), YESNO(1, "Yes No"), INPUT(2, "Text Input"),
        COLOR(3, "Color"), DATE(4, "Date");
        //, TEXTAREA(5,"textarea"), CHECKBOX(6,"checkbox");
        private final int mInputType;
        private final String mInputTypeText;

        INPUT_TYPE(int mInputType, String mInputTypeText) {
            this.mInputType = mInputType;
            this.mInputTypeText = mInputTypeText;
        }

        public int getValue() {
            return mInputType;
        }

        @Override
        public String toString() {
            return mInputTypeText;
        }
    }

    public enum ATTR_CAT {
        NON_PRICING(0), PRICING(1), DESCRIPTIVE(2), GENERAL(3);
        int value;

        ATTR_CAT(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public enum Image_Type {
        LEFT(0), RIGHT(1), TOP(4),
        BOTTOM(5), FRONT(2), BACK(3);

        private final int imageType;

        Image_Type(int imageType) {
            this.imageType = imageType;
        }

        public int getValue() {
            return imageType;
        }

    }
    
    public static final String ERROR_FIELD_MANDATORY = "Field is mandatory";
    public static final String ERROR_INVALID_PARAMETER = "Invalid parameter";
    public static final String ERROR_RESOURCE_NOT_FOUND = "Resource with id %1s not found";
    
    public static final String REGEXP_MOBILE = "^\\+[0-9]{1,3}\\.[0-9]{4,14}(?:x.+)?$";
    public static final String REGEXP_EMAIL = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";

}
