package com.rmcor.reale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RmcorRealeUtilApplication {

	public static void main(String[] args) {
		SpringApplication.run(RmcorRealeUtilApplication.class, args);
	}

}
